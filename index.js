const express = require('express')
var path = require('path');

const app = express()
const port = 3000

app.get('/server/server.js', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/server/server.js'));
});

app.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(port, () => console.log(`App is listening on port ${port}!`))